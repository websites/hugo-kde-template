# Template for hugo-kde

A template to create a website from [hugo-kde](https://invent.kde.org/websites/hugo-kde/).

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
